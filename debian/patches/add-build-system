diff --git a/Makefile b/Makefile
index 7a6b968d..9820bde9 100644
--- a/Makefile
+++ b/Makefile
@@ -1,15 +1,72 @@
 PREFIX := /usr
 INSTALLDIR := $(PREFIX)/share/libretro/assets
 
-all:
-	@echo "Nothing to make for retroarch-assets."
+GLUI_SRCDIR := src/glui
+GLUI_DIR := assets/glui
+GLUI_SVGS := $(wildcard $(GLUI_SRCDIR)/*.svg)
+GLUI_OUT := $(subst $(GLUI_SRCDIR),$(GLUI_DIR),$(subst .svg,.png,$(GLUI_SVGS)))
+
+WIDGETS_SRCDIR := src/menu_widgets
+WIDGETS_DIR := assets/menu_widgets
+WIDGETS_SVGS := $(wildcard $(WIDGETS_SRCDIR)/*.svg)
+WIDGETS_OUT := $(subst $(WIDGETS_SRCDIR),$(WIDGETS_DIR),$(subst .svg,.png,$(WIDGETS_SVGS)))
+
+XMB_MONOCHROME_SRCDIR := src/xmb/monochrome
+XMB_MONOCHROME_DIR := assets/xmb/monochrome
+XMB_MONOCHROME_SVGS := $(shell find $(XMB_MONOCHROME_SRCDIR) -iname '*.svg' | sed 's/ /\\ /g')
+XMB_MONOCHROME_OUT := $(subst $(XMB_MONOCHROME_SRCDIR),$(XMB_MONOCHROME_DIR)/png,$(subst .svg,.png,$(XMB_MONOCHROME_SVGS)))
+
+# References XMB_MONOCHROME so needs to come after it.
+OZONE_SRCDIR := ozone
+OZONE_DIR := assets/ozone
+OZONE_OUT := $(subst $(XMB_MONOCHROME_DIR)/png,$(OZONE_DIR)/png/icons,$(XMB_MONOCHROME_OUT))
+
+all: glui menu_widgets ozone xmb_monochrome
 
 install:
 	mkdir -p $(DESTDIR)$(INSTALLDIR)
-	cp -ar * $(DESTDIR)$(INSTALLDIR)
-	rm -rf $(DESTDIR)$(INSTALLDIR)/Makefile \
-		$(DESTDIR)$(INSTALLDIR)/configure \
-		$(DESTDIR)$(INSTALLDIR)/src
+	cp -a assets/* $(DESTDIR)$(INSTALLDIR)
+	cp -a branding rgui wallpapers $(DESTDIR)$(INSTALLDIR)
+	cp -a xmb/pixel $(DESTDIR)$(INSTALLDIR)/xmb
 
 test-install: all
 	DESTDIR=/tmp/build $(MAKE) install
+
+.PHONY: clean glui menu_widgets ozone xmb_monochrome
+clean:
+	rm -r assets || true
+
+$(GLUI_DIR):
+	mkdir -p $(GLUI_DIR)
+
+$(GLUI_DIR)/%.png: $(GLUI_SRCDIR)/%.svg $(GLUI_DIR)
+	inkscape -o "$@" "$<"
+
+glui: $(GLUI_DIR) $(GLUI_OUT)
+
+$(OZONE_DIR):
+	mkdir -p $(OZONE_DIR)/png/icons
+	cp -a ozone/png/* $(OZONE_DIR)/png/
+
+$(OZONE_DIR)/png/icons/%.png: $(XMB_MONOCHROME_SRCDIR)/%.svg $(OZONE_DIR)
+	debian/export-ozone "$<" "$@"
+
+ozone: $(OZONE_DIR) $(OZONE_OUT)
+
+$(WIDGETS_DIR):
+	mkdir -p $(WIDGETS_DIR)
+
+$(WIDGETS_DIR)/%.png: $(WIDGETS_SRCDIR)/%.svg $(WIDGETS_DIR)
+	inkscape -o "$@" "$<"
+
+menu_widgets: $(WIDGETS_DIR) $(WIDGETS_OUT)
+
+$(XMB_MONOCHROME_DIR)/png:
+	mkdir -p $(XMB_MONOCHROME_DIR)/png
+
+$(XMB_MONOCHROME_DIR)/png/%.png: $(XMB_MONOCHROME_SRCDIR)/%.svg $(XMB_MONOCHROME_DIR)/png
+	inkscape -C -w 256 -h 256 -o "$@" "$<"
+	mogrify -background 'rgb(255,255,255)' -alpha Background "$@"
+	optipng -o7 -strip all "$@"
+
+xmb_monochrome: $(XMB_MONOCHROME_DIR)/png $(XMB_MONOCHROME_OUT)
