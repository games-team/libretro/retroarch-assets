The DFSG tarball was built from a git checkout from
https://github.com/libretro/retroarch-assets dated 2022-10-24 (4ec80fa). As
some pieces needed to be removed due to licensing issues (largely around
commercial use), other extras were removed too in order to reduce the size of
the source tree.

In addition the Debian packaging adds a build system to try and recreate as
much as possible at build time from the source SVG files.

Actions performed:

 * Permissions fixes
   * find . -type f -name *.pdf -executable -exec chmod -x {} \;
   * find . -type f -name *.png -executable -exec chmod -x {} \;
   * find . -type f -name *.svg -executable -exec chmod -x {} \;
   * find . -type f -name *.txt -executable -exec chmod -x {} \;
   * chmod -x FlatUX/README.md

 * Renames
   * mv src/glui/playlist_tab_passive.svg src/glui/playlists_tab_passive.svg

 * Built artefact removales
   * rm -rf glui menu_widgets
   * rm ozone/png/icons/*.png ozone/*.ttf
   * rm -rf Automatic/backgrounds Automatic/icons
   * rm -rf FlatUX/backgrounds FlatUX/icons
   * rm -rf Systematic/backgrounds Systematic/icons

 * Non free (CC BY-NC 3.0) removals
   * rm -rf sounds src/sounds/
 * Non free (https://pixabay.com/en/service/license/) removals
   * rm rgui/wallpaper/fractal_spiral*
 * Non free (https://unsplash.com/license) removals
   * rm rgui/wallpaper/leaves*
   * rm rgui/wallpaper/mermaid_opal*
   * rm rgui/wallpaper/night_sky*
   * rm rgui/wallpaper/theatre_curtain*

 * No source removals (moved to
   https://github.com/baxysquare/baxy-retroarch-themes/tree/master/bytheme/NeoactiveRetired)
   * rm -rf src/xmb/neoactive/ xmb/neoactive/ src/xmb/retroactive/ xmb/retroactive/
   * rm -rf xmb/daite

 * Fonts
   * rm -rf fonts/ pkg/
   * rm src/ozone/mplus-1p-bold.ttf

 * Unneeded bits
   * rm -rf switch/ nxrgui/
   * rm -rf .git .gitlab-ci.yml .gitignore .github
